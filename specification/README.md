# ReportingSystemAPI

The API specification is written in [OpenAPI](https://www.openapis.org/),
which is based on [YAML](https://www.openapis.org/). To make the
specification more manageable, it is split across multiple files and
subdirectories. To names of the directories and files are based on the
OpenAPI structure and the domain (e.g., "guest"). So understanding the
structure required by OpenAPI will help in understanding the directory
structure.

Files are included into another files [using
`$ref:` entries](https://swagger.io/docs/specification/using-ref/).
[swagger-cli](https://www.npmjs.com/package/swagger-cli) is used to
"bundle" these files into a single file which is easier for clients
to consume.
[swagger-cli](https://www.npmjs.com/package/swagger-cli) is also used
to validate the specification.
