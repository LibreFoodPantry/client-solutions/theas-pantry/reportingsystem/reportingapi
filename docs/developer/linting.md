# linter

We use multiple tools to lint the parent project's source files.

## Requirements

Docker

## Local Usage

The commands can be run from anywhere. They are shown as running from the
root of the project:

```bash
bin/test.sh
```

## CI Usage

See [.gitlab-ci.yml](../../.gitlab-ci.yml)

## Linters

See bin/test.sh and .gitlab-ci.yml for a list of the linters we
are currently using.
