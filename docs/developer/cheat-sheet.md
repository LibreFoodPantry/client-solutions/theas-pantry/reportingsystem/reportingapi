# Developer Cheat Sheet

## Test

The backend is tested locally by running:

```bash
bin/test.sh
```

This runs `bin/test.sh` which will validate the OpenAPI specification using [Swagger/OpenAPI CLI](https://www.npmjs.com/package/swagger-cli).

These test are also run in the pipeline.

## Lint

Check all files meet standards. This command runs locally all linters that
will run in the pipeline on a push.

```bash
bin/lint.sh
```

## Squash commits to prepare for merge into main

Before merging a merge request, use the following command to squash its
commits into a single commit, writing a good conventional-commit message.

```bash
bin/premerge-squash.sh
```

## Prepare Release

Generate release artifacts for a specified version.

This script takes a version number as an argument and prepares release artifacts
for that version. It creates a directory structure under `./artifacts/release`
and generates an OpenAPI specification file named `openapi.yaml` with the
specified version number. The script performs a search and replace operation
within the original OpenAPI specification file to update the version number
to match the provided version.

```bash
bin/prepare-release.sh <version>
