# Build System

The build system is responsible for building the products
of this project, which is a bundled copy of the openapi
specification.

```bash
bin/build.sh
```

This runs `bin/build.sh` which generates `build/openapi.yaml` using [Swagger/OpenAPI CLI](https://www.npmjs.com/package/swagger-cli).
