# Reporting Systems Reporting API

The ReportingAPI is the API system for the reporting systems.

## Developer Guide

Getting Started

1. Read [LibreFoodPantry.org](https://librefoodpantry.org/)
    and join its Discord server.
2. [Install development environment](docs/developer/install-development-environment.md)
3. Clone this repository using the following command

    ```bash
    git clone <repository-clone-url>
    ```

4. Open it in VS Code and reopen it in a devcontainer.
5. Uses [ReportingAPI](https://gitlab.com/LibreFoodPantry/client-solutions/theas-pantry/reportingsystem/reportingapi) to set up the API for the ReportingSystems.
6. Familiarize yourself with the systems used by this project
  (see Development Infrastructure below).
7. See [the developer cheat-sheet](docs/developer/cheat-sheet.md) for common
  commands you'll likely use.

Development Infrastructure

* [Automated Testing](docs/developer/automated-testing.md)
* [Build System](docs/developer/build-system.md)
* [Continuous Integration](docs/developer/continuous-integration.md)
* [Dependency Management](docs/developer/dependency-management.md)
* [Development Environment](docs/developer/development-environment.md)
* [Documentation System](docs/developer/documentation-system.md)
* [Version Control System](docs/developer/version-control-system.md)
